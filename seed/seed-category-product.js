const Category = require('../models/category');
const Product = require('../models/product');
const slug = require('simple-slug');
const mongoose = require('mongoose');
const countCategory = 3;
const countProductsOfCategory = 12;
if(fs.existsSync('.env')){
    const dotenv = require('dotenv').config();
    if (dotenv.error) {
        throw dotenv.error
    }
}

async function run() {
    //connect to mongo db
    mongoose.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useCreateIndex: true
    });

    //init basic data
    const categoryList = [
        await new Category({title: 'Phones', slug: 'phones'}),
        await new Category({title: 'Tv', slug: 'tvs'}),
        await new Category({title: 'Laptop', slug: 'laptops'}),
    ];

    for (j = 0; j < countCategory; j++) {
        let newCategory = await categoryList[j].save();
        console.log(`new category ${categoryList[j].title} created`);
        for (i = 0; i < countProductsOfCategory; i++) {
            const title = await `Product ${i} from ${newCategory.title}`;
            const product = await new Product({
                title: title,
                price: Math.round(Math.random() * 1000),
                description: `some description of ${title}`,
                image: 'http://scemama.ch/user/themes/scemama-theme/images/no_img_avaliable.jpg',
                category: newCategory,
            });
            const newProduct = await product.save();
            console.log(`Create new product - ${product.title}, for category - ${newCategory.title}`);
        }
    }
    mongoose.disconnect();
}

run().catch(error => console.error(error.stack));