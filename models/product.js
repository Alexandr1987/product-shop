const mongoose = require('mongoose');
const slug = require('simple-slug');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    title:{
        required: true,
        type: String
    },
    price:{
        required: true,
        type: Number
    },
    slug: String,
    description: {
        type: String,
        trim: true
    },
    image: String,
    category: {
        type: mongoose.Schema.ObjectId,
        ref: 'categoryProduct',
        required: 'You must supply an category of product'
    }
});

//url for view product
ProductSchema
    .virtual('url')
    .get(function () {
        if(this.slug.length === 0){
            return '';
        }else{
            return '/product/' + this.slug;
        }
    });

// Define our indexes
ProductSchema.index({
    name: 'text',
    description: 'text',
    slug: 'text',
});


ProductSchema.pre('save', async function(next) {
    if (!this.isModified('title')) {
        next(); // skip it
        return; // stop this function from running
    }
    this.slug = await slug(this.title);
    // find other products that have a slug of wes, wes-1, wes-2
    const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
    const productsWithSlug = await this.constructor.find({ slug: slugRegEx });
    if (productsWithSlug.length) {
        this.slug = `${this.slug}-${productsWithSlug.length + 1}`;
    }
    next();
    // TODO make more resiliant so slugs are unique
});
module.exports  =  mongoose.model('Product', ProductSchema);