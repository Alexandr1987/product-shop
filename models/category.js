const mongoose = require('mongoose');
const slug = require('simple-slug');
const Schema = mongoose.Schema;

const categoryProductSchema = new Schema({
    title:{
        required: true,
        type: String
    },
    slug: String,
});

//url for view products of category
categoryProductSchema
    .virtual('url')
    .get(function () {
        if(this.slug.length === 0){
            return '';
        }else{
            return '/category/' + this.slug;
        }
    });

categoryProductSchema.pre('save', async function (next) {
    if (!this.isModified('title')) {
        next(); // skip it
        return; // stop this function from running
    }
    this.slug = await slug(this.title);
    next();
});

module.exports = mongoose.model('Category', categoryProductSchema);