const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const md5 = require('md5');
const bcrypt = require('bcryptjs');
const UserSchema = new Schema({
    googleId:{
        type: String
    },
    email:{
        required: true,
        type: String
    },
    username:{
        type: String
    },
    password:{
        required: true,
        type: String
    },
    role:{
        required: true,
        type: String,
        default: 'user'
    },
    date:{
        type: Date,
        default: Date.now(),
    }
});

UserSchema.getRoleAdmin = function(){
    return 'admin';
};

// UserSchema.isAdminUser = function(user){
//     return user.role === this.getRoleAdmin();
// };

UserSchema.virtual('isAdminUser').get(function() {
    return this.role === UserSchema.getRoleAdmin();
});


UserSchema.virtual('gravatar').get(function() {
    const hash = md5(this.email);
    return `https://gravatar.com/avatar/${hash}?s=200`;
});

UserSchema.pre('save', async function (next) {
    //bcrypt password if change password
    if (this.isModified('password')) {
        //crypt password
        const salt = await bcrypt.genSaltSync(10);
        this.password = await bcrypt.hashSync(this.password, salt);
    }

    next();
});
module.export  =  mongoose.model('User', UserSchema);