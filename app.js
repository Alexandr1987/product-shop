const fs = require('fs');
const createError = require('http-errors');
const express = require('express');
const path = require('path');
if(fs.existsSync('.env')){
    const dotenv = require('dotenv').config();
    if (dotenv.error) {
        throw dotenv.error
    }
}
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passport = require('passport');
const mongoose = require('mongoose');
//get role of user
const user = require('./models/users');
require('./models/product');
require('./models/category');
const hbs = require('hbs');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
require('./config/passport')(passport);
const flash = require('connect-flash');
const methodOverride = require('method-override');
const session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);
var store = new MongoDBStore({
    uri: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || process.env.MONGO_URI,
    collection: 'mySessions',
    databaseName: 'test',
});
// Catch errors
store.on('error', function(error) {
    console.log(error);
});

hbs.registerHelper('ifCond', function (v1, operator, v2, options) {
    console.log(`v1=${v1}|v=${v2}`);
    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this);
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});

hbs.registerHelper('categoryList', function (categories, categoryId) {
    if (!categories) {
        return '';
    }
    let template = ``;
    categories.forEach(item => {
        let activeItem = '';
        if (categoryId !== undefined) {
            activeItem = (item.id == categoryId ? 'selected' : '');
        }
        template += `<option ${activeItem}>${item.title}</option>`;
    });
    return template;
});

hbs.registerHelper('categoryListMenu', function (categories, category) {
    if (!categories) {
        return '';
    }
    let template = `<ul class="nav nav-pills card-header-pills"><div class="card-group">`;
    categories.forEach(item => {
        let activeItem = '';
        if (category !== undefined) {
            activeItem = (item.id === category.id ? 'active' : '');
        }
        template += `<li class="nav-item"><a class="nav-link ${activeItem}"  href="${item.url}">${item.title}</a></li>`;
    });
    template += `</div></ul>`;
    return template;
});

hbs.registerHelper('paginate', function (pagination, options) {
    if (!pagination) {
        return '';
    }

    var limit = 8
        , n;
    var queryParams = '';
    var page = pagination.page;
    var leftText = '<i class="fa fa-chevron-left">Previous</i>';
    var rightText = '<i class="fa fa-chevron-right">Next</i>';
    var paginationClass = 'pagination pagination-sm';

    if (options.hash.limit) limit = +options.hash.limit;
    if (options.hash.leftText) leftText = options.hash.leftText;
    if (options.hash.rightText) rightText = options.hash.rightText;
    if (options.hash.paginationClass) paginationClass = options.hash.paginationClass;

    var pageCount = Math.ceil(pagination.totalRows / pagination.limit);

    //query params
    if (pagination.queryParams) {
        queryParams = '&';
        for (var key in pagination.queryParams) {
            if (pagination.queryParams.hasOwnProperty(key) && key !== 'page') {
                queryParams += key + "=" + pagination.queryParams[key] + "&";
            }
        }
        var lastCharacterOfQueryParams = queryParams.substring(queryParams.length, -1);

        if (lastCharacterOfQueryParams === "&") {
            //trim off last & character
            queryParams = queryParams.substring(0, queryParams.length - 1);
        }
    }

    let template = `<nav aria-label="Page navigation justify-content-center"><ul class="pagination justify-content-center">`;
    var i = 0;
    var leftCount = Math.ceil(limit / 2) - 1;
    var rightCount = limit - leftCount - 1;
    if (page + rightCount > pageCount) {
        leftCount = limit - (pageCount - page) - 1;
    }
    if (page - leftCount < 1) {
        leftCount = page - 1;
    }
    var start = page - leftCount;

    while (i <limit && i < pageCount) {
        n = start;
        template = template + `<li class="page-item"><a class="page-link"  href="${pagination.prefix}/page/${n}">${n}</a></li>`;
        start++;
        i++;
    }
    template += `</ul></nav>`;
    return template;
});

const app = express();

mongoose.Promise = global.Promise;
//connect to mongo db
console.log(process.env.MONGO_URI);
mongoose.connect(process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || process.env.MONGO_URI, {
  useNewUrlParser:true,
  useCreateIndex: true
})
    .then(()=>{console.log('Connected to Mongodb');})
    .catch(error => {
      console.log(`Error:${error}`);
    });

app.set('view engine', 'hbs');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view options', {
    layout: 'main',
    layoutsDir: path.join(__dirname, 'views/layouts/'),
    defaultLayout:'main',
    partialsDir:path.join(__dirname, 'views/partials/'),
});
//set path to partials dir
hbs.registerPartials(__dirname + "/views/partials");
app.set("view engine", "hbs");

app.use(express.static(__dirname + "/public"));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// Takes the raw requests and turns them into usable properties on req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// override with POST having ?_method=DELETE
app.use(methodOverride('_method'));
// Exposes a bunch of methods for validating data. Used heavily on userController.validateRegister
app.use(expressValidator());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.set('trust proxy', 1);
app.use(session({
    secret: 'keyboard cat',store: store,resave: false,rolling: true,saveUninitialized: true,
    cookie: {maxAge: 10 * 60 * 1000,secure: true,httpOnly: false}
}));
// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());
// // The flash middleware let's us use req.flash('error', 'Shit!'), which will then pass that message to the next page the user requests
app.use(flash());
// pass variables to our templates + all requests
app.use((req, res, next) => {
    res.locals.success_main = req.flash('success') || null;
    res.locals.user = req.user || null;
    if(req.user){
        res.locals.isAdmin = req.user.isAdminUser;
    }else{
        res.locals.isAdmin = null;
    }
    res.locals.currentPath = req.path;
    next();
});
//use Routes
app.use('/', require('./routes/index'));
// done! we export it so we can start the site in start.js
module.exports = app;