const mongoose = require('mongoose');
const User = mongoose.model('User');
const passport = require('passport');

exports.google = async function(req, res) {
    passport.authenticate(
        'google',
        { scope: ['profile', 'email'] }
    )
};

exports.googleCallBack = function (req, res) {
    passport.authenticate(
        'google',
        { failureRedirect: process.env.REDIRECT_AFTER_ERROR_AUTH }
    )
};

exports.login =
    passport.authenticate('local', {
        failureRedirect: process.env.URL_LOGIN_USER,
        failureFlash: true,
        successRedirect: '/users/account',
        successFlash: true
    });

exports.logout = (req, res) => {
    req.logout();
    req.flash('success', 'You are now logged out!');
    res.redirect('/');
};

exports.register = async (req, res, next) => {
    const user = new User({ email: req.body.email, name: req.body.name, password: req.body.password});
    const register = promisify(User.register, User);
    await register(user, req.body.password);
    next(); // pass to authController.login
};


exports.userIsAdmin = async function(req, res, next){
    if (req.isAuthenticated() && res.locals.isAdmin){
        return next();
    }
    //not auth user
    req.flash('error', 'Not authorized!');
    if (req.isAuthenticated()){
        res.redirect('/users/account');
    }else{
        res.redirect(process.env.URL_LOGIN_USER);
    }
}

exports.userIsAuth = async function (req, res, next) {
    if (req.isAuthenticated()){
        return next();
    }
    //not auth user
    req.flash('error', 'Not authorized!');
    res.redirect(process.env.URL_LOGIN_USER);
};


exports.account = (req, res) => {
    res.render('account', { title: 'Edit Your Account' });
};