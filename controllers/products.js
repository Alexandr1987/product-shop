const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const User = mongoose.model('User');
const Category = mongoose.model('Category');

//view product not from admin panel
exports.getProductBySlug = async (req, res, next) => {
    const product = await Product.findOne({slug: req.params.slug});
    if (!product) return next();
    const category = await Category.findById(product.category);
    if (!category) return next();
    const breadcrumbs = [];
    breadcrumbs.push({name: 'Home', url: '/'});
    breadcrumbs.push({name: category.title, url: category.url});
    breadcrumbs.push({name: product.title, url: product.url});
    res.render('product/view', {
        product: product,
        title: product.title,
        breadcrumbs: breadcrumbs,
        category: category,
    });
};

//admin create and save new product
exports.saveProduct = async (req, res) => {
    //fill author, user is auth
    req.body.author = req.user._id;
    const product = await (new Product(req.body)).save();
    res.redirect(`/products/${product.slug}`);
};

//get form for create product
exports.createProductForm = async (req, res) => {
    const categories = await Category.find();
    res.render('product/admin/form', {
        title: 'Create new Product ',
        categories: categories,
        formAction: 'product/create'
    });
};

//edit product form
exports.editProductForm = async (req, res, next) => {
    const product = await Product.findById(req.params.id);
    if (!product) return next();
    const categories = await Category.find({});
    res.render('product/admin/form', {
        title: 'Update Product ',
        product: product,
        productTitle: product.id,
        formAction: 'product/update',
        categories: categories,
        categoryId: product.category,
    })
};

//update Product from admin panel
exports.updateStore = async (req, res) => {
    // find and update the store
    const product = await Product.findOneAndUpdate({_id: req.params.id}, req.body, {
        new: true, // return the new store instead of the old one
        runValidators: true
    }).exec();
    res.redirect(`/products/edit/id/${product._id}`);
};

//admin - get list products
exports.adminList = async (req, res, next) => {
    const perPage = parseInt(process.env.COUNT_PRODUCTS_ON_CATEGORY_PAGE);
    const page = req.params.page || 1;
    Product.find({})
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .exec(function (err, products) {
            Product.countDocuments({}).exec(function (err, count) {
                let pages = Math.ceil(count / perPage);
                let numbers = [];
                for (let k = 1; k <= pages; k++) {
                    numbers.push(k);
                }
                if (err) return next(err)
                res.render('product/admin/list', {
                    products: products,
                    title: 'Products:',
                    current: page,
                    pages: numbers,
                    pagination: {
                        page: page,
                        limit: perPage,
                        totalRows: count,
                        prefix: '/products',
                    }
                })
            })
        })
};

exports.deleteProduct = async (req, res, next) => {
    Product.deleteOne({_id: req.params.id}).then(product => {
        req.flash('success', 'Success delete product');
        res.redirect('/products');
    });
    next();
};

exports.homePage = (req, res) => {
    res.render('index');
};