const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const User = mongoose.model('User');
const Category = mongoose.model('Category');

//view category not from admin panel
exports.getCategoryBySlug = async (req, res, next) => {
    //find category by slug
    const category = await Category.findOne({slug: req.params.slug});
    if (!category) return next();
    const breadcrumbs = [];
    breadcrumbs.push({name: 'Home', url: '/'});
    breadcrumbs.push({name: category.title, url: category.url});
    const perPage = parseInt(process.env.COUNT_PRODUCTS_ON_CATEGORY_PAGE);
    const page = req.params.page || 1;
    const categories = await Category.find({});
    Product.find({category: category.id})
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .exec(function (err, products) {
            Product.countDocuments({category: category.id}).exec(function (err, count) {
                let pages = Math.ceil(count / perPage);
                let numbers = [];
                for (let k = 1; k <= pages; k++) {
                    numbers.push(k);
                }
                if (err) return next(err)
                res.render('category/view', {
                    breadcrumbs: breadcrumbs,
                    products: products,
                    category: category,
                    categories: categories,
                    title: category.title,
                    current: page,
                    pages: numbers,
                    pagination: {
                        page: page,
                        limit: perPage,
                        totalRows: count,
                        prefix: category.url,
                    }
                })
            })
        })
};

//admin create and save new category
exports.saveCategory = async (req, res) => {
    //fill author, user is auth
    const category = await (new Category(req.body)).save();
    res.redirect(`/category/${category.slug}`);
};

//get form for create category
exports.createCategoryForm = async (req, res) => {
    res.render('category/admin/form', {
        title: 'Create new Product Category',
        formAction: '/category/create',
        categoryTitle: '',
    });
};

//edit category form
exports.editCategoryForm = async (req, res, next) => {
    const category = await Category.findById(req.params.id);
    if (!category) {
        return next();
    }
    res.render('category/admin/form', {
        title: 'Update Category ',
        category: category,
        categoryTitle: category.id,
        slug: category.slug,
        formAction: `/category/edit/id/${category.id}`,
    })
};

//update Category from admin panel
exports.updateCategory = async (req, res, next) => {

    const category = await Category.findOneAndUpdate({_id: req.params.id}, {
        title: req.body.title,
        slug: req.body.slug
    }).exec();

    req.flash('success', ' Success update category');

    res.redirect(`/category`);
};
//validate params for update category
exports.validateUpdateCategory = async (req, res, next) => {
    const category = await Category.findById(req.params.id);
    console.log(category);
    req.sanitizeBody('slug');
    req.checkBody('title', 'You must supply a title!').notEmpty();
    req.checkBody('slug', 'That slug is not valid!').notEmpty();
    req.checkBody('slug', 'Slug already in use')
        .custom(value => {
            return Category.findOne({slug: value}).then(category => {
                if (category) {
                    if (category.id !== req.params.id) {
                        return Promise.reject('Category already in use');
                    }
                }
            });
        });

    const errors = await req.getValidationResult();
    if (!errors.isEmpty()) {
        res.render('category/admin/form', {
            errors: errors.array(),
            title: 'Update Category ',
            category: {title: req.body.title, slug: req.body.slug},
            formAction: `/category/edit/id/${category.id}`,
        });
        return;
    }
    next();// there were no errors!
};

//admin - delete category
exports.deleteCategory = async (req, res, next) => {
    Category.deleteOne({_id: req.params.id}).then(category => {
        req.flash('success', 'Success delete category');
        res.redirect('/category');
    });
    next();
};

//admin - validate form on create new category
exports.validateCreateCategory = async (req, res, next) => {
    req.sanitizeBody('slug');
    req.checkBody('title', 'You must supply a title!').notEmpty();
    req.checkBody('title', 'Category already in use!')
        .custom(value => {
            return Category.findOne({title: value}).then(category => {
                if (category) {
                    return Promise.reject('Category already in use');
                }
            });
        });
    req.checkBody('slug', 'That slug is not valid!').notEmpty();
    req.checkBody('slug', 'Slug already in use')
        .custom(value => {
            return Category.findOne({slug: value}).then(category => {
                if (category) {
                    return Promise.reject('Category already in use');
                }
            });
        });

    const errors = await req.getValidationResult();
    if (!errors.isEmpty()) {
        res.render('category/admin/form', {
            errors: errors.array(),
            title: 'Create Category ',
            category: {title: req.body.title, slug: req.body.slug},
            formAction: `/category/create`,
        });
        return;
    }
    next();// there were no errors!
};

exports.homePage = async (req, res) => {
    const categories = await Category.find({});
    const products = await Product.find({}).limit(parseInt(process.env.COUNT_PRODUCTS_ON_MAIN_PAGE));
    res.render('mainPage', {categories: categories, products: products});
};

exports.adminlist = async (req, res, next) => {
    //get categories from db
    const perPage = parseInt(process.env.COUNT_PRODUCTS_ON_CATEGORY_PAGE);
    const page = req.params.page || 1;
    Category.find({})
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .exec(function (err, categories) {
            Category.countDocuments({}).exec(function (err, count) {
                let pages = Math.ceil(count / perPage);
                let numbers = [];
                for (let k = 1; k <= pages; k++) {
                    numbers.push(k);
                }
                if (err) return next(err)
                res.render('category/admin/list', {
                    categories: categories,
                    title: 'Categories:',
                    current: page,
                    pages: numbers,
                    pagination: {
                        page: page,
                        limit: perPage,
                        totalRows: count,
                        prefix: '/category',
                    }
                })
            })
        })
};