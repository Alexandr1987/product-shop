const mongoose = require('mongoose');
const User = mongoose.model('User');
const bcrypt = require('bcryptjs');

exports.loginForm = (req, res) => {
    res.render('user/login', { title: 'Login user', errors_main: req.flash('error')});
};

exports.registerForm = (req, res) => {
    res.render('user/register', { title: 'Register user' });
};

exports.validateRegister = async (req, res, next) => {
    req.sanitizeBody('name');
    req.checkBody('name', 'You must supply a name!').notEmpty();
    req.checkBody('email', 'That Email is not valid!').isEmail();
    req.sanitizeBody('email')
        .normalizeEmail({
            gmail_remove_dots: false,
            remove_extension: false,
            gmail_remove_subaddress: false
        });
    req.checkBody('email', 'E-mail already in use')
        .custom(value => {
            return User.findOne({email: value}).then(user => {
                if (user) {
                    return Promise.reject('E-mail already in use');
                }
            });
        });
    req.checkBody('password', 'Password Cannot be Blank!')
        .notEmpty()
        .isLength({min: 6})
        .withMessage('Password must be at least 6 chars long');
    req.checkBody('password2', 'Confirmed Password cannot be blank!').notEmpty();
    req.checkBody('password2', 'Oops! Your passwords do not match').equals(req.body.password);

    const errors = await req.getValidationResult();
    if(!errors.isEmpty()){
        res.render('user/register', {
            errors: errors.array(),
            title: 'Register',
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            password2: req.body.password2
        });
        return;
    }
    next();// there were no errors!
};

exports.register = async (req, res, next) => {
    const user = new User({ email: req.body.email, username:req.body.name, password:req.body.password });
    const resultSaveUser = await user.save();
    next(); // pass login
};

exports.account = (req, res) => {
    res.render('user/profile', { title: 'Your Account' });
};