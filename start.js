// import all of our models
require('./models/product');
require('./models/users');
require('./models/category');

// Start app
const app = require('./app');
app.set('port', process.env.PORT || 5000);
const server = app.listen(app.get('port'), () => {
    console.log(`Express running → PORT ${server.address().port}`);
});