const mongoose = require('mongoose');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const LocalStrategy = require('passport-local').Strategy;
const User = mongoose.model('User');
const bcrypt = require('bcryptjs');

module.exports = function(passport){
    passport.use(new GoogleStrategy({
            clientID: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
            callbackURL: "/auth/google/callback",
            proxy: true,
        },
        function(accessToken, refreshToken, profile, done) {
            User.findOrCreate({ googleId: profile.id }, function (err, user) {
                return done(err, user);
            });
        }
    ));

    passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
            session: true
        },
        function(req, username, password, done) {
            // Find user in db
            User.findOne({ email: username }, function (err, user) {
                if (err) {
                    req.flash('error', 'User not found');
                    return done(err, { message: 'User not find.' });
                }
                if (!user) {
                    req.flash('error', 'User not found');
                    return done(null, false);
                }
                const isValidPassword = bcrypt.compareSync(password, user.password);
                if (!isValidPassword) {
                    req.flash('error', 'Password is not correct');
                    return done(null, false);
                }
                req.flash('success', 'You are now logged in!');
                return done(null, user);
            });
        }
    ));

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
            return null;
        });
    });
};