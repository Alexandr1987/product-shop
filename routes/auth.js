const express = require('express');
const router = express.Router();
const passport = require('passport');

router.get('/google',
    passport.authenticate(
        'google',
        { scope: ['profile', 'email'] }
    )
);

router.get('/google/callback',
    passport.authenticate(
        'google',
        { failureRedirect: process.env.REDIRECT_AFTER_ERROR_AUTH }
        ),
    (req, res)=>{
        // Successful authentication, redirect home.
        res.redirect(process.env.REDIRECT_AFTER_SUCCESS_AUTH);
    });

module.exports = router;