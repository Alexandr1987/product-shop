const createError = require('http-errors');
const express = require('express');
const router = express.Router();

const productController = require('../controllers/products');
const usersController = require('../controllers/users');
const authController = require('../controllers/auth');
const categoryController = require('../controllers/category');

// requests will never reach this route
router.get('/', categoryController.homePage);
//get product page by slug
router.get('/product/:slug', productController.getProductBySlug);
//get category page by slug with pagination
router.get('/category/:slug/page/:page', categoryController.getCategoryBySlug);
//get category with products by slug
router.get('/category/:slug', categoryController.getCategoryBySlug);
//get login form
router.get(process.env.URL_LOGIN_USER, usersController.loginForm);
//try to login on site
router.post(process.env.URL_LOGIN_USER, authController.login);
//get form register
router.get(process.env.URL_REGISTER_USER, usersController.registerForm);
//registration user - action
router.post(process.env.URL_REGISTER_USER,
    //Validate the registration data
    usersController.validateRegister,
    //register the user
    usersController.register,
    //we need to log them in
    authController.login
);
router.get('/users/logout', authController.userIsAuth, authController.logout);
router.get('/users/account', authController.userIsAuth, usersController.account);
//admin - create new category
router.get('/category/create', authController.userIsAuth, authController.userIsAdmin, categoryController.createCategoryForm);
//admin - save new category
router.post('/category/create', authController.userIsAuth, authController.userIsAdmin, categoryController.validateCreateCategory, categoryController.saveCategory);
//admin - get form for edit category
router.get('/category/edit/id/:id', authController.userIsAuth, authController.userIsAdmin, categoryController.editCategoryForm);
//admin - update category, save updated  data
router.post('/category/edit/id/:id', authController.userIsAuth, authController.userIsAdmin, categoryController.validateUpdateCategory, categoryController.updateCategory);
//admin - delete category - with related products
router.delete('/category/id/:id', authController.userIsAuth, authController.userIsAdmin, categoryController.deleteCategory);
//admin - get categories list
router.get('/category', authController.userIsAuth, authController.userIsAdmin, categoryController.adminlist);
//admin - get category list with pagination
router.get('/category/page/:page', authController.userIsAuth, authController.userIsAdmin, categoryController.adminlist);

router.get('/products/create', authController.userIsAuth, authController.userIsAdmin, productController.createProductForm);
//admin - remove product
router.delete('/products/id/:id', authController.userIsAuth, authController.userIsAdmin, productController.deleteProduct);
//admin - get edit product form
router.get('/products/edit/id/:id', authController.userIsAuth, authController.userIsAdmin, productController.editProductForm);
//admin - get products - for crud table
router.get('/products', authController.userIsAuth, authController.userIsAdmin, productController.adminList);

router.get('/products/page/:page', authController.userIsAuth, authController.userIsAdmin, productController.adminList);

// catch 404 and forward to error handler
router.use(function(req, res, next) {
  next(createError(404));
});

// error handler
router.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
module.exports = router;